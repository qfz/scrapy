# Scrapy框架爬取
## 利用scrapy框架爬取笔趣阁

## 1.  网页原理分析
- 利用scrapy爬取书籍信息，获取其书籍根地址
- 找到目录标签，分节网址，网页跳转结构
## 2.  数据抽取
- 修改items.py文件
- 定义抽取的标题，内容
## 3.  自定义spider
- 请求根地址，获得响应数据
- 对响应数据进行数据解析
- 对目录地址进行回调，请求详情页，提取文本
- 返回所提取的数据给管道文件
## 4.  修改settings.py文件
- 设置请求头，放开管道，以及爬取间隔时间设置
## 5.  数据存储
此处保存为csv文档，也可保存至MongDB、mysql

